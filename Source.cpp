#include <iostream>
#include "SFML\Audio.hpp"
using namespace std;

int main() {
	string cnt;
	float vol;
	bool loop;
	string sng; //value meant to be where FILENAME is
	cout << "Make sure there is only .ogg files in the play folder before typing 'play'.\n"; cin >> cnt;
	if (cnt == "play")
	{
		cout << "File Name and path. Supports ogg and wav.\n"; cin >> sng;
		cout << "Set Volume (0-100):\n"; cin >> vol;
		cout << "Loop? (true/false)\n"; cin >> loop;
		sf::Music music;
		if (!music.openFromFile("play\\(FILENAME).ogg"))
			cout << "Double check your spelling.'E' to exit"; cin >> cnt;
		if (cnt == "e") {
			return -1; // error
		}
		music.setVolume(vol);
		music.setLoop(loop);
		music.play();
	}
	else {
		cout << "No Play detected. Exiting...";
	}
}